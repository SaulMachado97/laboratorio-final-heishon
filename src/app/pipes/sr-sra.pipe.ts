import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'srSra'
})
export class SrSraPipe implements PipeTransform {

  transform(value: string, genero: string): string {
    return genero === 'M' ? 'Mr. ' + value : 'Miss. ' + value;
  }

}
