import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalleUsuarioComponent } from './detalle-usuario/detalle-usuario.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { FormularioCustomerComponent } from './formulario-customer/formulario-customer.component';
import { HomeComponent } from './home/home.component';
import { InicioComponent } from './inicio/inicio.component';
import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { ListaCustomersComponent } from './lista-customers/lista-customers.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';

const routes: Routes = [
  {
    path: '',
    component: InicioComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'usuarios',
        component: ListaUsuariosComponent
      },
      {
        path: 'usuarios/:id',
        component: DetalleUsuarioComponent
      },
      {
        path: 'empleados',
        component: EmpleadoComponent
      },
      {
        path: 'clientes',
        component: ListaClientesComponent
      },
      {
        path: 'customers',
        component: ListaCustomersComponent
      },
      {
        path: 'customers/add',
        component: FormularioCustomerComponent
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
