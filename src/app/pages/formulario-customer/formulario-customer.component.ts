import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CustomersService} from '../../services/customers.service';

@Component({
  selector: 'app-formulario-customer',
  templateUrl: './formulario-customer.component.html',
  styleUrls: ['./formulario-customer.component.css']
})
export class FormularioCustomerComponent implements OnInit {

  frmCustomer: FormGroup;

  constructor(
    private frm: FormBuilder,
    private customerService: CustomersService
  ) { }

  ngOnInit(): void {
    this.frmCustomer = this.frm.group({
      CustomerID: ['', [Validators.required, Validators.minLength(3)]],
      CompanyName: ['', Validators.required],
      ContactName: ['', Validators.required],
      ContactTitle: ['', Validators.required],
      Address: ['', Validators.required],
      City: ['', Validators.required],
      Region: ['', Validators.required],
      PostalCode: ['', [Validators.required, Validators.maxLength(6)]],
      Country: ['Colombia', Validators.required],
      Phone: ['', Validators.required],
      Fax: ['', Validators.required]
    });
  }

  onSubmit(): any{
    console.log(this.frmCustomer.value);
    const customer = this.frmCustomer.value;
    this.customerService.CreateCustomer(customer);
    this.frmCustomer.reset();
  }



}
