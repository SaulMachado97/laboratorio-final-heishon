import { Component, OnInit } from '@angular/core';
import { Customer } from 'src/app/model/customer';
import {CustomersService} from '../../services/customers.service';

@Component({
  selector: 'app-lista-customers',
  templateUrl: './lista-customers.component.html',
  styleUrls: ['./lista-customers.component.css']
})
export class ListaCustomersComponent implements OnInit {

  customers: Array<Customer> = [];

  constructor(private customerService: CustomersService) { }

  ngOnInit(): void {
    // this.customerService.ObtenerCustomers().subscribe(d => console.log(d));
    this.customers = this.ConsultarCustomers();
  }

  ConsultarCustomers(): any{
    console.log('Consultando customers');
    this.customerService.ObtenerCustomers()
      .subscribe((lstCustomers: Array<Customer>) => {
        this.customers = lstCustomers;
        console.log(this.customers);
      });
  }

}
