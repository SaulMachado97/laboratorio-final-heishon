import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { InicioComponent } from './inicio/inicio.component';
import { SharedModule } from '../shared/shared.module';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { HomeComponent } from './home/home.component';
import { GeneroPipe } from '../pipes/genero.pipe';
import { SrSraPipe } from '../pipes/sr-sra.pipe';
import { FiltroUsuariosComponent } from './filtro-usuarios/filtro-usuarios.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetalleUsuarioComponent } from './detalle-usuario/detalle-usuario.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { ListaCustomersComponent } from './lista-customers/lista-customers.component';
import { FormularioCustomerComponent } from './formulario-customer/formulario-customer.component';


@NgModule({
  declarations: [InicioComponent, ListaUsuariosComponent, HomeComponent, GeneroPipe, SrSraPipe, FiltroUsuariosComponent, DetalleUsuarioComponent, EmpleadoComponent, ListaClientesComponent, ListaCustomersComponent, FormularioCustomerComponent],
  imports: [
    CommonModule,
    FormsModule,
    PagesRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class PagesModule { }
