import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  frmEmpleado: FormGroup;
  constructor(private frm: FormBuilder) { }

  ngOnInit(): void {
    this.frmEmpleado = this.frm.group({
      nombreColaborador: ['', [Validators.required, Validators.minLength(3)]],
      direccion: ['', Validators.required],
      telefono: ['', Validators.required],
      correo: ['', [Validators.required, Validators.email]],
      ciudad: [''],
      salario: [''],
      nacionalidad: ['co']
    });
  }

  guardar(): void{
    console.log('Guardado');
    this.frmEmpleado.reset();
  }

}
