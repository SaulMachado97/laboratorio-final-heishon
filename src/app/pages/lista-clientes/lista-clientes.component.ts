import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../../services/clientes.service';
import { Cliente } from '../../model/cliente';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {

  clientes: Array<Cliente> = [];

  constructor(private clienteService: ClientesService) { }

  ngOnInit(): void {
    this.clienteService.ObtenerClientes(10).subscribe(d => console.log(d));
  }

  consultarClientes(cantClientes: number) {
    if (cantClientes) {
      console.log(cantClientes);
      this.clienteService.ObtenerClientes(cantClientes)
        .subscribe((lstClients: Array<Cliente>) => {
          this.clientes = lstClients;
        });
    } else {
      alert('Ingrese la cantidad de clientes que desea visualizar');
      this.clientes = [];
    }
  }

}
