import { Component, OnInit } from '@angular/core';
import {Paises} from '../paises';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.css']
})
export class PaisesComponent implements OnInit {

  paises : Array<Paises> = [
    {
      nombre: "Colombia",
      capital: "Bogota",
      poblacion: "50.34 millones",
      moneda: "pesos"
    },
    {
      nombre:"Francia",
      capital: "Paris",
      poblacion: "44.94 millones",
      moneda: "euro"
    },
    {
      nombre:"EEUU",
      capital: "Washington",
      poblacion: "100.94 millones",
      moneda: "dolar"
    },
    {
      nombre:"España",
      capital: "Madrid",
      poblacion: "60.94 millones",
      moneda: "euro"
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
