export class Usuario{
  cedula: number | string;
  genero: string;
  nombre: string;
  direccion: string;
  telefono: string;
  fechaNacimiento: Date;
  salario: number;
}
