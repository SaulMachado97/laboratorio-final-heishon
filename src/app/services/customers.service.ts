import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Customer} from '../model/customer';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  readonly url = environment.urlServicioCustomers;

  constructor( private http: HttpClient) { }

  ObtenerCustomers(): Observable<any>{
    const urlCustomer = `${this.url}`;
    const lstCustomers: Array<Customer> = [];
    return this.http.get<any>(urlCustomer).pipe(
      map(clientes => {
        (clientes as Array<any>).forEach((registro: any) => {
          lstCustomers.push({
            CustomerID: registro.CustomerID,
            CompanyName: registro.CompanyName,
            ContactName: registro.ContactName,
            ContactTitle: registro.ContactTitle,
            Address: registro.Address,
            City: registro.City,
            Region: registro.Region,
            PostalCode: registro.PostalCode,
            Country: registro.Country,
            Phone: registro.Phone,
            Fax: registro.Fax
          });
        });
        return lstCustomers;
      })
    );
  }

  CreateCustomer(customer: any){
    const urlCustomer = `${this.url}`;
    return this.http.post(urlCustomer, customer).subscribe();
  }
}
